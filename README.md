# YEAP [DEPRICATED]
### *yet another podcast-manager*

# DEPRECATED in favor of a re-write in Rust. [https://gitlab.gnome.org/alatiera/Hammond](https://gitlab.gnome.org/alatiera/Hammond)

### Dependencies:

* Requires Python 3.6+.

* **[youtube_dl](https://rg3.github.io/youtube-dl/)**
* **[records](https://github.com/kennethreitz/records)**
* **[feedparser](https://github.com/kurtmckee/feedparser)**
* **[pyxdg](https://freedesktop.org/wiki/Software/pyxdg/)**
* **[listparser](https://github.com/kurtmckee/listparser)**
* **[lxml](https://github.com/lxml/lxml)**


## Features:

* OPML Import/Export Support
* Http Etag and Last Modified Header Support
* Complete RSS 2.0 Support (Need Feedback for Atom)
* Youtube RSS channel feeds support
* Youtube-dl configuration exposed, Thus complete ffmpeg transcode support etc

## Usage:

```
git clone https://github.com/alatiera/yeap.git
python3 -mpip install yeap/ # Might require sudo
yeap --help
```

#### first add some feeds:
` yeap add example.com/feed.xml `

#### then reflesh
` yeap update` or

` yeap up`

#### and then download
` yeap download 3` or

` yeap dl 3`

this will downlaod the latest 3 episodes of every feed

## Save location and download options:
The default save location is `~/Podcasts` and can be changed by replacing the
 `"save_loc"` parameter in the `config.json` file.

 Downloads are powered by [youtube_dl](https://rg3.github.io/youtube-dl/),
 you can add parameters by changing the `"ydl_opts"` parameters.
 Available options can be found [here](https://github.com/rg3/youtube-dl/blob/master/youtube_dl/YoutubeDL.py#L129-L279)


## Further options:

#### List the 10 latest shows:

`yeap -l`

#### List sources:

`yeap --list-sources`

#### Remove a feed:
Removes a feed and everythign associated with it.
Give it the ID you will get from `--list-sources`

`yeap --rm-feed [int]`

#### Import/Export your sources:

`yeap --import-opml foobar.opml]`

`yeap --export-opml foobar.opml

#### Index as much episodes as you want to the db:
By default it indexes the last 10 episodes, but you can override it

`yeap --index [int]`

#### Reset the database:
Soft reset will clean the whole database to except the source table, aka your RSS feed list
`yeap --soft-reset`

Hard reset will throw the database to the sun, and replace it with a clean one.
`yeap --hard-reset`

#### Print some information of any feed:
`yeap --info [url]`

**example:**

`yeap --info https://feeds.feedburner.com/linuxunogg`

```
Feed version: rss20

----- Feed Info -----
Feed Title: LINUX Unplugged OGG
Link: http://www.jupiterbroadcasting.com/show/linuxun/
Description: An open show powered by community LINUX Unplugged takes the best attributes of open collaboration and focuses them into a weekly lifestyle show about Linux.
Published: Tue, 14 Mar 2017 19:10:39 -0700
Image: http://www.jupiterbroadcasting.com/images/LASUN-Badge1400.jpg
Author: Jupiter Broadcasting (linuxactionshow@jupiterbroadcasting.com)
Language: en
Rights: Copyright Jupiter Broadcasting

----- Item Info -----
Title: Celebrating Linux on Pi Day | LUP 188
Description: We celebrate Pi Day by loading Mycroft & Alexa onto a Raspberry Pi 3, look at the actual use cases for VR & AR under Linux today, flash back to Linux in the 90s & update on our favorite projects.
Link: http://www.jupiterbroadcasting.com/107511/celebrating-linux-on-pi-day-lup-188/
Published: Tue, 14 Mar 2017 19:09:52 -0700
Source: http://www.podtrac.com/pts/redirect.ogg/traffic.libsyn.com/jnite/lup-0188.ogg
Type: audio/ogg
Length: 50863099
Comments: None

----- Item Info -----
Title: CIA's Dank Trojans | LUP 187
Description: <p>It’s the year of the CIA linux desktop, with multiplatform malware & boot environments designed to attack Macs, Popey & Wimpy share their Mobile World Congress adventures & Bryan joins us to discuss the last Linux Sucks talk ever.</p>
<p>Plus we chat with Gnome at SCALE, take a look at Endless OS & ponder the Litebook.</p>
Link: http://www.jupiterbroadcasting.com/107336/cias-dank-trojans-lup-187/
Published: Tue, 07 Mar 2017 20:12:47 -0800
Source: http://www.podtrac.com/pts/redirect.ogg/traffic.libsyn.com/jnite/lup-0187.ogg
Type: audio/ogg
Length: 68377258
Comments: None
```

## `yeap --help`
```
usage: yeap [-h] [-l] [--info INFO] [--soft-reset] [--hard-reset]
            [--list-sources] [--import-opml IMPORT_OPML]
            [--export-opml EXPORT_OPML] [--rm-feed RM_FEED] [--index INDEX]
            {add,update,up,download,dl} ...

positional arguments:
  {add,update,up,download,dl}
    add                 add the url to sources list
    update (up)         Check and retrieve new content
    download (dl)       download [num] latest shows from every feed

optional arguments:
  -h, --help            show this help message and exit
  -l, --latest          lists the 10 latests episodes
  --info INFO           Prints Feed info
  --soft-reset          Resets sqlite database but not your feeds sources
  --hard-reset          Nuke the whole db out of orbit
  --list-sources        Lists your feed sources
  --import-opml IMPORT_OPML
                        Import feed sources from opml
  --export-opml EXPORT_OPML
                        Exports feed sources to opml
  --rm-feed RM_FEED     Removes [id] feed and its associated items
  --index INDEX         Indexes [int] latest items/episodes
```
