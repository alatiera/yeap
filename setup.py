from setuptools import setup, find_packages


with open('README.md') as f:
    README = f.read()

with open('LICENSE') as f:
    LICENSE = f.read()

setup(
    name='yeap',
    description='A cli podcast manager',
    author='Jordan Petridis',
    author_email='alatiera@openmailbox.org',
    url='https://github.com/alatiera/yeap',
    version='0.0.2',
    long_description=README,
    license=LICENSE,
    install_requires=['feedparser', 'youtube-dl', 'lxml'
                      'records', 'pyxdg', 'listparser'],
    packages=find_packages(exclude=['tests']),
    scripts=['yeap'],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.6',
    ]

)
