from time import gmtime
from src import parse_feeds

# TODO add more values and more diverse
EPOCH_VALUES = {
    1497327508: '2017-06-13', 1494276651: '2017-05-08',
    1491434125: '2017-04-05', 1487761200: '2017-02-22',
    1493287200: '2017-04-27', 698287300: '1992-02-17',
    0: '1970-01-01', -1: '1969-12-31', -42: '1969-12-31',
    -424242: '1969-12-27', -42424242: '1968-08-27',
    -4242424242: '1835-07-25', -42424242424: '625-08-18'
}


# TODO specific 301 check
def test_check_status():

    # TODO cover more status results
    http_codes = {302: False, 304: True, 410: True}

    # normal result
    for code, result in http_codes.items():
        assert parse_feeds.check_status(code) == result

    # None case
    assert parse_feeds.check_status(None) is True

    # On unknown codes it should default to False
    assert parse_feeds.check_status(999) is False
    assert parse_feeds.check_status(42) is False
    assert parse_feeds.check_status(666) is False


def test_get_epoch():

    # Normal epoch values test case
    for i in EPOCH_VALUES:
        time_struct = gmtime(i)
        assert parse_feeds.get_epoch(time_struct) == i

    # None check
    assert parse_feeds.get_epoch(None) is None
    assert parse_feeds.get_epoch('None') is None


def test_get_date():

    # Normal epoch values test case
    for epoch, date in EPOCH_VALUES.items():
        assert parse_feeds.get_date(epoch) == date

    # None check
    assert parse_feeds.get_date(None) is None
