import os
from functools import partial

import records

from . import YEAP_DATA, logger

CREATE_AUTHOR = """
    CREATE TABLE `author` (
    `id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    `author`	TEXT NOT NULL UNIQUE
    );
                """

CREATE_COP = """
    CREATE TABLE `copyright` (
    `id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    `cp_holder`	TEXT NOT NULL UNIQUE
    );
             """


CREATE_LANG = """
    CREATE TABLE `language` (
    `id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    `language`	TEXT NOT NULL UNIQUE
    );
             """

CREATE_TYPE = """
    CREATE TABLE `type` (
    `id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    `type`	TEXT NOT NULL UNIQUE
    );
              """

CREATE_SOURCE = """
    CREATE TABLE IF NOT EXISTS `source` (
    `id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    `url`	TEXT NOT NULL UNIQUE,
    `etag`	TEXT,
    `modified`	TEXT
    );
                """


CREATE_TAG = """
    CREATE TABLE `tag` (
    `id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    `term`	TEXT NOT NULL UNIQUE
    );
             """

CREATE_FTAGS = """
    CREATE TABLE `feedtags` (
    `feed_id`	INTEGER,
    `tag_id`	INTEGER,
    PRIMARY KEY (feed_id , tag_id)
    );
               """

CREATE_CONTRY = """
    CREATE TABLE `content_entry` (
    `entry_id`	INTEGER,
    `content_id`	INTEGER,
    PRIMARY KEY(`entry_id`,`content_id`)
    );
                """

CREATE_CONTENT = """
    CREATE TABLE `content` (
    `id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    `value`	TEXT UNIQUE,
    `base`	TEXT,
    `language_id`	INTEGER,
    `type_id`	INTEGER
    );
                 """

CREATE_FEED = """
    CREATE TABLE `feed` (
    `id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    `title`	TEXT NOT NULL UNIQUE,
    `link`	TEXT,
    `description`	TEXT,
    `published`	TEXT,
    `epoch`	INTEGER,
    `image`	TEXT,
    `source_id`	INTEGER UNIQUE,
    `author_id`	INTEGER,
    `language_id`	INTEGER,
    `rights_id`	INTEGER
    );
              """

CREATE_ENTRY = """
    CREATE TABLE `entry` (
    `id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    `title`	TEXT,
    `description`	TEXT,
    `published`	TEXT,
    `epoch`	INTEGER,
    `href`	TEXT UNIQUE,
    `thumbnail`	TEXT,
    `length`	INTEGER,
    `entryid`	TEXT,
    `comments`	TEXT,
    `feed_id`	INTEGER,
    `local_uri`	TEXT
    );
               """

TABLES = {
    'author': CREATE_AUTHOR,
    'copyright': CREATE_COP,
    'language': CREATE_LANG,
    'type': CREATE_TYPE,
    'source': CREATE_SOURCE,
    'tag': CREATE_TAG,
    'feedtags': CREATE_FTAGS,
    'content_entry': CREATE_CONTRY,
    'content': CREATE_CONTENT,
    'feed': CREATE_FEED,
    'entry': CREATE_ENTRY
}


# Probably can setup a test.db file for unit testing that way
def dbcon(file_='feeds.db') -> records.Database:
    """Return a records.Database object."""
    db_path = os.path.join(YEAP_DATA, file_)
    if os.path.exists(db_path):
        return records.Database(f'sqlite:///{db_path}')

    dbobj = records.Database(f'sqlite:///{db_path}')
    hardreset(dbobj)
    return dbobj


def create_table(dbobj: records.Database, tablename: str) -> None:
    """Run the create function of :tablename, must be in the TABLES dict."""
    tran = dbobj.transaction()
    if not TABLES.get(tablename):
        logger.error('table {tablename} not found in the tablelist')
        return

    dbobj.query(TABLES.get(tablename).strip())
    logger.info(f'{tablename} Created')
    tran.commit()


def drop_table(dbobj: records.Database, table: str) -> None:
    """Drop :table from the database."""
    tran = dbobj.transaction()
    dbobj.query(f'DROP TABLE IF EXISTS {table}')
    logger.info(f'{table} Dropped')
    tran.commit()


drop_author = partial(drop_table, table='author')
drop_copyright = partial(drop_table, table='copyright')
drop_language = partial(drop_table, table='language')
drop_type = partial(drop_table, table='type')
drop_source = partial(drop_table, table='source')
drop_tag = partial(drop_table, table='tag')
drop_feedtags = partial(drop_table, table='feedtags')
drop_content_entry = partial(drop_table, table='content_entry')
drop_content = partial(drop_table, table='content')
drop_feed = partial(drop_table, table='feed')
drop_entry = partial(drop_table, table='entry')


def hardreset(soft: bool = False) -> None:
    """Reset the database to a clean state.

    If soft is True, Keep the source table intact.
    """
    logger.info(f'reseting db..., soft value = {soft}')
    dbobj = dbcon()
    tran = dbobj.transaction()

    for table in TABLES:
        # Reset the whole database expet the source table
        if table == 'source' and soft is True:
            # clean all the etag/mod tags so it will refetch the feeds after
            dbobj.query('UPDATE source set etag=Null, modified=Null')
            continue

        drop_table(dbobj, table)
        create_table(dbobj, table)

    tran.commit()
    return


softreset = partial(hardreset, soft=True)
