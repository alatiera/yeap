import feedparser as fp
from . import parse_feeds


def feedinfo(feed: parse_feeds.FEEDTUP) -> None:
    """Print the contents of the FeedTup of the Rss feed."""
    # Based on RSS 2.0 Spec
    # https://cyber.harvard.edu/rss/rss.html

    print('\n----- Feed Info -----')

    # Common elements
    print(f'Feed Title: {feed.title}')
    print(f'Link: {feed.link}')
    print(f'Description: {feed.description}')
    print(f'Published: {feed.published}')
    # print('Published Parsed:{}'.format(feedobj.feed.get('published_parsed')))

    # Uncommon elements
    if feed.image:
        print('Image: {}'.format(feed.image.get('href')))

    # print('Categories: {}'.format(feedobj.feed.get('categories')))
    # print('Cloud: {}'.format(feedobj.feed.get('cloud')))

    # Extra
    print(f'Author: {feed.author}')
    print(f'Language: {feed.language}')
    print(f'Rights: {feed.copyright}')


def iteminfo(entry: parse_feeds.ENTRYTUP, content: bool = True) -> None:
    """Print the contents of the Item object of the feed."""
    print('\n----- Item Info -----')

    # Common elements
    print(f'Title: {entry.title}')
    print(f'Description: {entry.description}')
    print(f'Link: {entry.link}')
    print(f'Published: {entry.published}')
    # print(f'Published Parsed: {entry.published_parsed}')
    # print(f'ID: {entry.id}')

    # Uncommon elements

    # Enclosures
    # print(f'Enclosures: {entry.enclosures}')
    print(f'Source: {entry.uri}')
    print(f'Type: {entry.type}')
    print(f'Length: {entry.length}')

    # Content
    if content and entry.content is not None:
        print(f'Contents: {entry.content}')

        for content in entry.content:
            con = parse_feeds.content_data(content)
            entrycontent(con)

    print(f'Comments: {entry.comments}')


def entrycontent(content: parse_feeds.CONTUP) -> None:
    """Print the data of entry.content."""
    print('\n----- Content Info -----')
    print(f'Content Base: {content.base}')
    print(f'Content Type: {content.type}')
    print(f'Content Value: {content.value}')
    print(f'Content Base: {content.language}')


def fullprint(feeduri: str, limit: int = 3) -> None:
    """Print the data of the :feeduri feed, :limit limits the enties result."""
    feed = fp.parse(feeduri)
    print('Feed version: {}'.format(feed.get('version')))

    feed_ = parse_feeds.feed_data(feed)
    feedinfo(feed_)

    for ent in feed_.entries[:limit]:
        entry = parse_feeds.entry_data(ent)
        iteminfo(entry)


def main() -> None:
    """Ask for a url and print the contents of the rss feed."""
    url = input('Insert Feed URL: ')
    # Something Something Sanitize the input
    fullprint(url)


if __name__ == '__main__':
    main()
