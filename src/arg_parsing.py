import argparse
import logging

from . import (dboperations, dbstructure, downloader, feed_indexing, logger,
               opml, rssinfo)


# TODO make --help more human friendly
# TODO write tests
def cli_parse():

    # TODO improve argparse
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    parser.add_argument('-l', '--latest', help='lists the 10 latests episodes',
                        action='store_true')
    debuglvls = ['INFO', 'DEBUG', 'ERROR', 'CRITICAL', 'WARNING']
    parser.add_argument('-d', '--debug', help='Set the Logging level.',
                        choices=debuglvls)

    parser.add_argument('--info', help='Prints Feed info')

    parser.add_argument('--soft-reset', help="""Resets sqlite database but not your
        feeds sources""", action='store_true')

    parser.add_argument('--hard-reset', help='Nuke the whole db out of orbit',
                        action='store_true')

    parser.add_argument('--list-sources', help='Lists your feed sources',
                        action='store_true')

    parser.add_argument('--import-opml', help='Import feed sources from opml',
                        type=argparse.FileType('r', encoding='UTF-8'))

    parser.add_argument('--export-opml', help='Exports feed sources to opml',
                        type=argparse.FileType('w', encoding='UTF-8'))

    parser.add_argument('--rm-feed', help="""Removes [id] feed and its
            associated items""", type=int)

    parser.add_argument('--index', help='Indexes [int] latest items/episodes',
                        type=int)

    # subparsers
    add = subparsers.add_parser('add', help='add the url to sources list')
    add.add_argument('add', help='add [url]', type=str)

    update = subparsers.add_parser('update', aliases=['up'],
                                   help='Check and retrieve new content')

    update.add_argument('up', action='store_true')

    download = subparsers.add_parser('download', help="""download [num] latest
            shows from every feed""", aliases=['dl'])

    download.add_argument('num', help='Number of backlog you want to index',
                          type=int)

    args = parser.parse_args()
    return args


def main():
    args = cli_parse()

    if args.debug:
        # print(f'lvl before {logger.getEffectiveLevel()}')
        dlevel = getattr(logging, args.debug.upper())
        # print(dlevel)
        logger.setLevel(dlevel)
        # print(f'lvl after {logger.getEffectiveLevel()}')

    if args.latest:
        print(dboperations.print_latest(dbstructure.dbcon()))

    if args.info:
        rssinfo.fullprint(args.info)

    if args.soft_reset:
        dbstructure.softreset()

    if args.hard_reset:
        dbstructure.hardreset()

    if args.list_sources:
        source = dboperations.list_source(dbstructure.dbcon())
        if source.all() == []:
            print('There are not any feeds currently')
        else:
            print(source.dataset)

    if args.import_opml:
        # read the opml file
        file = args.import_opml.read()
        dbobj = dbstructure.dbcon()

        # Not sure if its needed
        # dbstructure.softreset()

        # import the urls to source table
        for feed in opml.opml_uris(file):
            feed_indexing.source_index(dbobj=dbobj, url=feed)

        # update the db after the import
        feed_indexing.index_feeds()

    if args.export_opml:
        dbobj = dbstructure.dbcon()
        opml.opml_export(dbobj=dbobj, file=args.export_opml)

    if args.rm_feed:
        dboperations.annihilate_feed(dbstructure.dbcon(), args.rm_feed)

    if args.index:
        feed_indexing.index_feeds(limit=args.index, force=True)

    if 'add' in args:
        feed_indexing.source_index(dbstructure.dbcon(), args.add)
        print('{} added!'.format(args.add))

    if 'up' in args:
        feed_indexing.index_feeds()

    if 'num' in args:
        downloader.latest_dl(dbstructure.dbcon(), args.num)
