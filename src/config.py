import json
import os
from typing import Dict

from . import YEAP_CONFIG


def load_config() -> Dict:
    """Return the content of the settings.json file."""
    confpath = os.path.join(YEAP_CONFIG, 'settings.json')
    if not os.path.exists(confpath):
        default_config(confpath)

    with open(confpath) as conf:
        config = json.load(conf)
        return config


def default_config(path_: os.PathLike):
    """Write the a default settings.json template to :path_ ."""
    template = {
        'save_loc': '~/Podcasts',
        'ydl_opts': {
            "quiet": True,
            "format": "bestaudio/best"
        }
    }

    with open(path_, 'w') as conf:
        json.dump(template, conf)
