from calendar import timegm
from collections import deque
from time import gmtime, strftime, struct_time
from typing import Deque, Dict, Iterator, List, NamedTuple, Tuple

import feedparser as fp

from . import dboperations, dbstructure, logger

FEEDTUP = NamedTuple('Feed', [('title', str), ('description', str),
                              ('link', str), ('published', str),
                              ('published_parsed', struct_time),
                              ('image', str), ('etag', str), ('modified', str),
                              ('author', str), ('language', str),
                              ('copyright', str),
                              ('tags', List[fp.FeedParserDict]),
                              ('entries', List[fp.FeedParserDict])])

ENTRYTUP = NamedTuple('Entry', [('title', str), ('description', str),
                                ('published', str), ('thumbnail', str),
                                ('published_parsed', struct_time),
                                ('id', str), ('comments', str),
                                ('uri', str), ('length', int),
                                ('content', Dict), ('type', str)])

CONTUP = NamedTuple('Content', [('base', str), ('value', str),
                                ('language', str), ('type', str)])


def uri_deque() -> Deque[Tuple[str, str, str]]:
    """Return a collections.deque with source's uri, etag, modified fields."""
    dbobj = dbstructure.dbcon()

    queue = deque()  # type: Deque
    source = dboperations.pure_source(dbobj)

    for rec in source:
        foobar = (rec.url, rec.etag, rec.modified)
        queue.append(foobar)

    return queue


def fetch_feeds(force: bool = False)-> Iterator[Tuple[fp.FeedParserDict, str]]:
    """Yield a feedparser dict, source url for every item in the Queue."""
    print('Fetching feeds...')

    que = uri_deque()
    lenq = len(que)

    for count, foobar in enumerate(que.copy(), start=1):
        url, etag, mod = foobar

        if force:
            # Gets the feed xml no matter what
            feed = fp.parse(url)
            logger.debug(f'Made Request to {url}')
        else:
            # Sents etag/mod with the request, Recieves 304 if nothing changed
            feed = fp.parse(url, etag=etag, modified=mod)
            logger.debug(
                f'Made Request to {url} with etag={etag} and mod={mod}')

        que.pop()
        logger.info(f'Retrieved {count} of {lenq}')
        print(f'Retrieved {count} of {lenq}')

        yield feed, url


def feed_data(feed: fp.FeedParserDict) -> FEEDTUP:
    """Return a namedtuple with the parsed / sanitized contents of the feed."""
    title = feed.get('feed').get('title')
    etag = feed.get('etag', None)
    mod = feed.get('modified', None)
    desc = feed.feed.get('description', None)
    link = feed.feed.get('link', None)
    pub = feed.feed.get('published', None)
    pubpar = feed.feed.get('published_parsed', None)
    img = feed.feed.get('image', None)

    # TODO add file extension check, like if indeed a jpg or png
    if img:
        imghref = feed.feed.image.get('href', 'None')
    else:
        imghref = None

    # if isinstance(img, fp.FeedParserDict):
    #     imguri = feed.feed.image.get('href', None)

    auth = feed.feed.get('author', 'Not provided')
    lang = feed.feed.get('language', 'Not provided')
    rights = feed.feed.get('rights', 'Not provided')

    # Needed when making db fk
    # TODO make the 'Not provided' blanks back None, and leave it on the client
    # side to render it as Not provided
    if rights == '':
        rights = 'Not provided'

    tags = feed.feed.get('tags', None)

    entries = feed.get('entries', None)

    if desc == '':
        desc = None

    foobar = FEEDTUP(title=title, etag=etag, modified=mod, description=desc,
                     link=link, published=pub, published_parsed=pubpar,
                     image=imghref, author=auth, language=lang,
                     copyright=rights, tags=tags, entries=entries)

    return foobar


def entry_data(entry: fp.FeedParserDict) -> ENTRYTUP:
    """Return a namedtuple with the parsed / sanitized contents of an Entry."""
    title = entry.get('title', None)
    desc = entry.get('description', None)
    pub = entry.get('published', None)
    pubpar = entry.get('published_parsed', None)
    entryid = entry.get('id', None)
    comm = entry.get('comments', None)

    links = entry.get('links')

    # Some feed place a first item with the site's url
    if len(links) > 1:
        links = links[1]
    else:
        links = links[0]

    href = links.get('href', None)
    length = links.get('length', None)
    ftype = links.get('type', None)

    cont = entry.get('content', None)

    thumb = None

    if 'yt' in entryid:
        thumb = entry.media_thumbnail[0].get('url')

    foobar = ENTRYTUP(title=title, description=desc, published=pub, uri=href,
                      published_parsed=pubpar, id=entryid, length=length,
                      thumbnail=thumb, comments=comm, content=cont, type=ftype)

    return foobar


def content_data(content: fp.FeedParserDict) -> CONTUP:
    """Return a namedtuple with parsed / sanitized data of a content item."""
    base = content.get('base', None)
    value = content.get('value', None)
    contype = content.get('type', None)
    lang = content.get('language', None)

    if lang is None:
        lang = 'Not provided'
    else:
        lang = lang.lower()

    foobar = CONTUP(base=base, value=value, language=lang, type=contype)

    return foobar


def check_status(feed: fp.FeedParserDict, feeduri: str=None) -> bool:
    """Check the http header returned by the feedparser obj.

    301: Permanent redirect of the url
    302: Temporary redirect of the url
    304: Up to date Feed, checked with the Etag
    410: Feed deleted
    """
    status = feed.get('status')
    href = feed.href

    if feed.bozo == 1:
        # TODO inform user etc
        logger.error(f'bozo exception: {feed.bozo_exception}')
        return True

    if not status:
        return True

    # Permanent redirect, updating db
    elif status == 301:
        logger.info(f'Permanent feed redirect from {feeduri} to {href}')
        dboperations.replace_source_uri(feeduri, href)
        return False

    # temporary redirect, nothing to do here'
    elif status == 302:
        return False

    # Feed up to date
    elif status == 304:
        return True

    # TODO Feed deleted, inform user
    elif status == 410:
        return True

    return False


# PS. Every time related thing is in UTC by default thanks to feedparser,
def get_epoch(pubpar: struct_time) -> int:
    """Return the UTC epoch time of a time.struct_time or Tuple."""
    if pubpar is None or pubpar == 'None':
        epoch = None

    else:
        try:
            epoch = timegm(pubpar)

        # Just in case someone has weird feeds
        except (TypeError, ValueError) as err:
            logger.error(str(err))
            logger.error(f"Something went wrong, pubpar value = {pubpar}")
            epoch = None

    return epoch


def get_date(epoch: int) -> str:
    """Return a YY - MM - DD UTC formated string from an: epoch timestamp."""
    if epoch is None or not isinstance(epoch, int):
        return None

    parsed_struct = gmtime(epoch)
    date = strftime('%Y-%m-%d', parsed_struct)
    return date
