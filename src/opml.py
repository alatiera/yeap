from email.utils import formatdate
from typing import Iterator

import listparser as lp
from lxml import etree
from records import Database

from _io import TextIOWrapper

from . import dboperations, logger


# TODO refactor to drop listparser dependency, probably use xml module
# listparser seems unmaintained
def opml_uris(file: TextIOWrapper) -> Iterator[str]:
    opml = lp.parse(file)
    feeds = opml.feeds
    bozo = opml.bozo

    if bozo is 1:
        logger.error('Error while importing from OPML')
        logger.error(opml.bozo_exception)
        return

    for feed in feeds:
        yield feed.url


def opml_export(dbobj: Database, file: TextIOWrapper)-> None:
    """Export the source db table as opml to :file file."""
    # Havent found the proper way to add the doc notation atm
    # xml = etree.Element('xml', version='1.0', encoding='UTF-8')
    file.write('<?xml version="1.0" encoding="utf-8"?>\n')

    doc = etree.Element('opml', version='2.0')

    tree = etree.ElementTree(doc)
    head = etree.SubElement(doc, 'head')
    body = etree.SubElement(doc, 'body')

    title = etree.SubElement(head, 'title')
    title.text = 'Yeap subscriptions'
    date = etree.SubElement(head, 'dateCreated')
    date.text = formatdate(localtime=True)

    for feed in dboperations.source_export(dbobj):
        ftitle: str = feed.title
        url: str = feed.url
        txt: str = feed.description
        if txt is None:
            txt = ""

        outline = etree.SubElement(body, 'outline', text=txt,
                                   title=ftitle, type='rss',
                                   xmlUrl=url)

    # etree.dump(tree)
    treestr = etree.tostring(tree, encoding='unicode', pretty_print=True)
    file.write(treestr)
