from os import path
from typing import Dict

import youtube_dl
from records import Database, RecordCollection

from . import parse_feeds as parf
from . import dboperations, config, logger


def get_opts(dest: str, date: str = None) -> Dict:
    """Configure and return the yt_dl file save options."""
    conf = config.load_config().copy()
    opts = conf.get('ydl_opts').copy()

    if date:
        # saves the file with the date provided by the Feed
        savetmpl = date + '-%(title)s.%(ext)s'
    else:
        # fallback to date provided by the file
        savetmpl = '%(upload_date)s-%(title)s.%(ext)s'

    destination = path.join(conf.get('save_loc'), dest, savetmpl)
    opts['outtmpl'] = destination
    return opts


def ytdl(url: str, opts: dict) -> None:
    """Download the given :url, pass :opts as the ytdl options file."""
    with youtube_dl.YoutubeDL(opts) as ydl:
        logger.info(f'Downloading from {url}')
        ydl.download([url])


def download(recobj: RecordCollection) -> None:
    """Handle the download naming an saving of an entry object.

    With a record object/collecion with feed.title, entry.href and
    entry.epoch values, pass them to youtube_dl for downloading.
    """
    # print(recobj.dataset)
    for i in recobj:
        # print(i.title)
        # print(i.href)
        # print(i.date)
        date = parf.get_date(i.epoch)
        ytdl(i.href, get_opts(i.title, date))


def latest_dl(dbobj: Database, limit: int = 3) -> None:
    """Iterate through every feed in the db and download latest entries."""
    feed_id = dbobj.query('select id from feed')
    for i in feed_id:
        rec = dboperations.latest_shows(dbobj, i.id, limit)

        if rec.all():
            # print(rec.dataset)
            download(rec)
