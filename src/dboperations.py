import tablib
from records import Database, RecordCollection
from sqlalchemy.exc import IntegrityError

from . import dbstructure

"""This module is a mess that never got loved, It just get the job done atm."""
# TODO update docstrings


def print_latest(dbobj: Database) -> RecordCollection:
    """Return a tablib dataset with entry.title, feed.title and entry.pub."""
    data = dbobj.query("""select entry.title, feed.title, entry.published from entry
            join feed on entry.feed_id = feed.id
            order by entry.epoch DESC limit 10""")
    headers = ('Episode Title', 'Feed', 'Published')

    if data.all() == []:
        print("Sorry your list is empty")
        return

    # Replace headers
    data = tablib.Dataset(*data.dataset, headers=headers)
    return data


def latest_shows(dbobj: Database, feed_id: str,
                 limit: int = 3) -> RecordCollection:
    """Return feed.title, entry.href, entry.epoch.

    feed_id: Should correspond to the feed.id object in the database
    limit: Specifies the backlog of items returned, ordered by date
    """
    urls = dbobj.query("""
        select
            feed.title,
            entry.href,
            entry.epoch
        from
            entry
            join
                feed
        where
            entry.feed_id = feed.id
            and feed.id = :feed_id
        order by
        entry.epoch desc limit :limit""",
                       feed_id=feed_id, limit=limit)
    return urls


def list_source(dbobj: Database) -> RecordCollection:
    """Return records object of source.id, source.url and feed.title."""
    source = dbobj.query("""
            select
                feed.id,
                feed.title,
                source.url
            from
                source
                join
                    feed
            where
                source.id = feed.source_id
            order by
                feed.id
                    """)
    return source


def pure_source(dbobj: Database) -> RecordCollection:
    """Return a records object with all the contents of the soutce table."""
    source = dbobj.query('select * from source')
    return source


def fid_from_sid(dbobj: Database, source_id: str) -> str:
    """Return feed.id associated with source.id."""
    feed_id = dbobj.query("""
            select
                feed.id
            from
                feed
                join
                    source
            where
                feed.source_id = source.id
                and source.id = :source_id
                        """, source_id=source_id)
    return feed_id


def annihilate_feed(dbobj: Database, feed_id: str) -> None:
    """Take care of removing :feed_id from the database.

    Also removes entries, tags relations.
    """
    source_id = dbobj.query("""select source.id from feed join source where
            feed.source_id = source.id and feed.id = :feed_id""",
                            feed_id=feed_id)[0]

    annihilate_entries(dbobj, feed_id)
    annihilate_tag_rel(dbobj, feed_id)

    dbobj.query('delete from feed where feed.id = :feed_id', feed_id=feed_id)
    dbobj.query('delete from source where id= :source_id',
                source_id=source_id.id)


def annihilate_entries(dbobj: Database, feed_id: str) -> None:
    """Remove the items related to :feed_id from the entries table."""
    ids = dbobj.query('select id from entry where entry.feed_id = :feed_id',
                      feed_id=feed_id)
    for i in ids:
        annihilate_contry_rel(dbobj, i.id)
    dbobj.query('delete from entry where feed_id = :feed_id', feed_id=feed_id)


def annihilate_contry_rel(dbobj: Database, entry_id: str) -> None:
    """Remove the times related to :entry_id from the content_entry table."""
    con_ids = dbobj.query("""select content_id from content_entry where
            entry_id = :entry_id""", entry_id=entry_id)
    # print(con_ids.dataset)
    for i in con_ids:
        # dbobj.query('delete from content where id = :con_id',
        #             con_id=i.content_id)

        dbobj.query("""delete from content_entry where content_id = :con_id
                    and entry_id = :entry_id""", con_id=i.content_id,
                    entry_id=entry_id)


def annihilate_tag_rel(dbobj: Database, feed_id: str) -> None:
    """Remove the times related to :feed_id from the feedtags table."""
    dbobj.query('delete from feedtags where feed_id=:feed_id', feed_id=feed_id)


def replace_source_uri(olduri: str, newuri: str) -> None:
    """Update source table feed entry of olduri to newuri."""
    dbobj = dbstructure.dbcon()
    tran = dbobj.transaction()
    with tran:
        dbobj.query('update source set url = :newuri where url = :olduri',
                    newuri=newuri, olduri=olduri)


def update_feed(dbobj: Database, source_id: str, title: str) -> None:
    # FIXME Refactor
    tran = dbobj.transaction()
    try:
        dbobj.query("""insert into feed(title, source_id) values(:title,
            :source_id)""", source_id=source_id, title=title)
    except IntegrityError:
        dbobj.query("""update feed set source_id = :source_id where
            title = :title""", source_id=source_id, title=title)
    finally:
        tran.commit()


def source_export(dbobj: Database) -> RecordCollection:
    query = """
SELECT title,
       description,
       source.url
FROM   feed
JOIN   source
where  feed.source_id = source.id
""".strip()
    return dbobj.query(query)
