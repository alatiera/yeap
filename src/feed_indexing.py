from functools import partial
from typing import List

from feedparser import FeedParserDict
from records import Database, Record, RecordCollection
from sqlalchemy.exc import IntegrityError

from . import parse_feeds as parf
from . import dboperations, dbstructure


def single_str_index(dbobj: Database, value: str, table: str,
                     column: str) -> Record:
    """Index a value and return its recods object.

    Inserts a value into the given table and column,
    and then it returns the records object of the value.
    """
    tran = dbobj.transaction()
    ins_query = f'INSERT OR IGNORE INTO {table}({column}) VALUES(:value)'
    dbobj.query(ins_query, value=value)
    sel_query = f'SELECT id FROM {table} WHERE {column} = :value'
    ret = dbobj.query(sel_query, value=value)
    tran.commit()
    return ret[0]


author_index = partial(single_str_index, table='author', column='author')
lang_index = partial(single_str_index, table='language', column='language')
rights_index = partial(single_str_index, table='copyright', column='cp_holder')
type_index = partial(single_str_index, table='type', column='type')
tag_index = partial(single_str_index, table='tag', column='term')


# FIXME Refactor
def source_index(dbobj: Database, url: str, etag: str = None,
                 mod: str = None) -> RecordCollection:
    """Insert/Update the source table, return a records objerct of the row."""
    tran = dbobj.transaction()
    try:
        dbobj.query("""insert into source(url, etag, modified)
                values(:url, :etag, :mod)""", url=url, etag=etag, mod=mod)
    except IntegrityError:
        dbobj.query("""update source set etag = :etag, modified = :mod
                where url = :url""", etag=etag, mod=mod, url=url)
    finally:
        tran.commit()

    source = dbobj.query("""select * from source where url = :url""",
                         url=url)[0]
    return source


def feed_index(feed: parf.FEEDTUP, dbobj: Database) -> None:
    """Index contents of a Feedtup to the database."""
    epoch = parf.get_epoch(feed.published_parsed)

    tran = dbobj.transaction()
    exists = dbobj.query('select title from feed where title=:title',
                         title=feed.title)

    if exists.all() is None:
        dbobj.query("""INSERT INTO feed (title, description, link, published,
                    epoch, image)
                    VALUES(:title, :desc, :link, :pub, :epoch, :img)""",
                    title=feed.title, desc=feed.description, link=feed.link,
                    pub=feed.published, img=str(feed.image), epoch=epoch)
    else:
        dbobj.query("""update feed set description=:desc, link=:link,
                    published=:pub, epoch=:epoch,
                    image=:img where title=:title""",
                    desc=feed.description, link=feed.link, pub=feed.published,
                    img=feed.image, epoch=epoch, title=feed.title)

    tran.commit()
    add_feed_fkeys(feed, dbobj)


def add_feed_fkeys(feed: parf.FEEDTUP, dbobj: Database) -> None:
    """Create the foreighn keys relations of the Feed table."""
    tran = dbobj.transaction()

    # foreign keys

    # check if exists in its tables if not insert it and return key's id
    auth_id = author_index(dbobj, feed.author).id
    lang_id = lang_index(dbobj, feed.language).id
    rights_id = rights_index(dbobj, feed.copyright).id

    # create fk relations
    dbobj.query("update feed set author_id = :auth_id where title = :title",
                auth_id=auth_id, title=feed.title)
    dbobj.query("update feed set language_id = :lang_id where title = :title",
                lang_id=lang_id, title=feed.title)
    dbobj.query("update feed set rights_id= :rights_id where title = :title",
                rights_id=rights_id, title=feed.title)

    tran.commit()

    if feed.tags:
        # foobar = partial(tag_index, dbobj=dbobj)

        # Theres probably a better way to exhaust the map
        # list(map(foobar, feed.feed.tags))

        [tag_index(dbobj, x.term) for x in feed.tags]

        mk_ftags_rel(feed, dbobj)


def mk_ftags_rel(feed: parf.FEEDTUP, dbobj: Database) -> None:
    """Create the foreighn key relation of feed and tags tables."""
    feed_id = dbobj.query('select id from feed where title = :title',
                          title=feed.title)[0][0]

    foobar = partial(feedtags_index, dbobj=dbobj, feed_id=feed_id)
    # Theres probably a better way to exhaust the map
    list(map(foobar, feed.tags))


def feedtags_index(tag: FeedParserDict, dbobj: Database, feed_id: str) -> None:
    """Index items to the feedtags table."""
    tran = dbobj.transaction()
    term = tag.get('term')
    tag_id = dbobj.query('select id from tag where term = :term',
                         term=term)[0][0]
    dbobj.query("""insert or ignore into feedtags(feed_id, tag_id)
                VALUES(:feed_id, :tag_id)""",
                feed_id=feed_id, tag_id=tag_id)
    tran.commit()


def entry_index(entry: parf.ENTRYTUP, dbobj: Database) -> None:
    """Index the contents of feedparser.entries dict to the Database."""
    epoch = parf.get_epoch(entry.published_parsed)

    tran = dbobj.transaction()
    dbobj.query("""
        INSERT OR IGNORE INTO entry (title, description, href,
        published, epoch, length, entryid, comments)
        VALUES(:title, :desc, :href, :pub, :epoch, :length, :entryid,
        :comm)  """, title=entry.title, desc=entry.description,
                pub=entry.published, epoch=epoch,
                href=entry.uri, length=entry.length, entryid=entry.id,
                comm=entry.comments)

    tran.commit()


def add_entry_fks(entry: parf.ENTRYTUP, dbobj: Database,
                  feedlink: str) -> None:
    """Create the foreighn key relations of the entry table."""
    tran = dbobj.transaction()

    # I know its ugly
    try:
        feed_id = dbobj.query('select id from feed where link = :link',
                              link=feedlink)[0][0]

        dbobj.query("update entry set feed_id = :feed_id where href = :href",
                    feed_id=feed_id, href=entry.uri)

    except IntegrityError as err:
        print('feed_indexing, add_entry_fks Error: ', err)

    finally:
        tran.commit()


def content_index(content: List[FeedParserDict], dbobj: Database) -> None:
    """Iterate through the feed.entries[x].content dict and indexes it."""
    if content is None:
        return

    foobar = partial(actual_content, dbobj=dbobj)
    # Somethimes there more than 1 items on the list, so you need to iterate
    # over it
    # Theres probably a better way to exhaust the map
    list(map(foobar, content))


def actual_content(content: FeedParserDict, dbobj: Database) -> None:
    """Indexes feed.entries[x].content to the content table."""
    tran = dbobj.transaction()
    con = parf.content_data(content)

    # check if exists in its tables if not insert it, return key's id
    type_id = type_index(dbobj, con.type).id
    lang_id = lang_index(dbobj, con.language).id

    dbobj.query("""insert or ignore into content(base, value,
                type_id, language_id) values(:base, :value,
                :type_id, :lang_id)""", base=con.base, value=con.value,
                type_id=type_id, lang_id=lang_id)
    tran.commit()


def mk_contry_rl(entry: parf.ENTRYTUP, dbobj: Database) -> None:
    """Create the content and entry database key relations."""
    entry_id = dbobj.query('select id from entry where href = :href',
                           href=entry.uri)[0][0]

    if entry.content is not None:
        foobar = partial(content_entry_index, entry_id=entry_id, dbobj=dbobj)
        # Theres probably a better way to exhaust the map
        list(map(foobar, entry.content))


def content_entry_index(con: FeedParserDict, entry_id: str,
                        dbobj: Database) -> None:
    """Create the content-entry relation."""
    tran = dbobj.transaction()
    value = parf.content_data(con).value
    content_id = dbobj.query("""select id from content where
                                value = :value""", value=value)[0][0]
    dbobj.query("""insert or ignore into content_entry(entry_id,
            content_id) VALUES(:entry_id, :content_id)""",
                entry_id=entry_id, content_id=content_id)
    tran.commit()


def complete_index(feed: parf.FEEDTUP, dbobj: Database, limit=10) -> None:
    """Do a complete index of a Feed Tuple (parse_feeds.FEEDTUP)."""
    feed_index(feed, dbobj)

    for entry in feed.entries[:limit]:
        entry = parf.entry_data(entry)

        entry_index(entry, dbobj)
        content_index(entry.content, dbobj)
        add_entry_fks(entry, dbobj, feed.link)
        mk_contry_rl(entry, dbobj)


def index_feeds(limit: int = 10, force: bool = False) -> None:
    """Full index/update of the feeds and episodes in the db.

    limit: specify the number of backlog to be indexed
    force: Skip Etag/modified http tags
    """
    dbobj = dbstructure.dbcon()  # type: Database

    for feed, url in parf.fetch_feeds(force):

        # Update the source table entry of the given feed
        feed_ = parf.feed_data(feed)
        source = source_index(dbobj, url, feed_.etag, feed_.modified)

        # Hacky update of the feeds table feed items
        dboperations.update_feed(dbobj, source.id, feed_.title)

        # Skips if check_status returns True
        if parf.check_status(feed, feeduri=url):
            continue

        # Do a complete index of the feed tuple
        complete_index(feed_, limit=limit, dbobj=dbobj)
