import logging
from os import makedirs, path
from contextlib import suppress

from xdg import BaseDirectory


YEAP_DATA = path.join(BaseDirectory.xdg_data_home, 'yeap')
YEAP_CONFIG = path.join(BaseDirectory.xdg_config_home, 'yeap')
YEAP_CACHE = path.join(BaseDirectory.xdg_cache_home, 'yeap')


def xdg_setup() -> None:
    """Check existance/Setup the neccesary config files."""

    if not path.exists(path.join(YEAP_DATA)):
        with suppress(FileExistsError):
            makedirs(YEAP_DATA)

    if not path.exists(path.join(YEAP_CONFIG)):
        with suppress(FileExistsError):
            makedirs(YEAP_CONFIG)

    if not path.exists(YEAP_CACHE):
        with suppress(FileExistsError):
            makedirs(YEAP_CACHE)


xdg_setup()
logging.basicConfig(level=logging.INFO,
                    format='%(levelname)s:%(asctime)s:%(message)s',
                    filename=path.join(YEAP_CACHE, 'yeap.log'))
logger = logging.getLogger('init_logger')
